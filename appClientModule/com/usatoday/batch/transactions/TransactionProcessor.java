package com.usatoday.batch.transactions;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.mutable.MutableInt;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.ssh.SSHFTPClient;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.businessObjects.subscriptionTransactions.SubscriberTransactionBatchBO;
import com.usatoday.businessObjects.util.ZeroFilledNumeric;
import com.usatoday.businessObjects.util.mail.EmailAlert;

/**
 *  This process requires the USA TODAY model code. As such it requires the usat_application.properties file for configuration as
 *  well as it's own batch.properties file.
 *  
 *  The default location for the usat_application.properties file is /usat/
 *   
 *    This method attempts to load the usat_application.properties file from one of 
 *     these places in the followig order:
 *     1.   from the file specified in the System property com.usatoday.usatAppPropertiesFile
 *     2.   From the file system in /USAT/extranet/usat_application.properties
 *     3.   /com/usatoday folder of classpath
 *     4.   current directory that the USATApplicationPropertyFileLoader was loaded from
 *     5.   From the /WEB-INF folder
 *     
 * @author aeast
 *
 */
public class TransactionProcessor {

    private String propertyFileName = null; // batch.properties
    
    private int numberOfDaysBeforePurge = 14;
    private String logFileDirName = ".";
    private String dataOutputDirName = ".";
	private String baseOuputFileName = "XD";
	private String gpgCommand = "gpg";
    private String gpgHomeDir = "c:\\gnupg";
    private String gpgKeyAlias = "aeast@usatoday.com";
    private String FTPHost = "159.54.128.31";
    private String ftpUserId = "extftptest";
    private String ftpPwd = "19extftptest99";
    private String sftpHost = "ftps.gannett.com";
    private String sftpUserId = "usat-webportaltest";
    private String sftpPwd = "pBIsm9vS5JSiZsGFpecr";
    private String useSftp = "Y";
    private boolean encryptPlainTextFile = true;
    private boolean deletePlainTextFile = true;
    private boolean deleteArchiveFile = true;
    private boolean sendReport = true;
    private String reportRecipients = null;
    private String alertRecipients = "aeast@usatoday.com";
    
    private HashMap<String, HashMap<String, MutableInt>> processingStats = new HashMap<String, HashMap<String,MutableInt>>();
    
    private String batchID = null;
    
    // flag that allows the doWork method to execute only one time per instance
    private boolean workDone = false;
    private boolean batchMode = false;
    
    private DateTime startTime = null;
    private DateTime endTime = null;
    
    private int recordsProcessed = 0;
    
    private ArrayList<String> alertMessages = new ArrayList<String>();
    private ArrayList<String> processingMessages = new ArrayList<String>();
    
    private Logger processLog = null;
    private Level  logLevel = Level.WARNING;

    public static final String useString = "Useage: TransactionProcessor (-help) -propertyFile fileName (-mode [reprocess|normal] -batchid [required if mode=reprocess, batchid of batch to process from column XTRNTDWNLD.batch_id]) ";
    
	public static void main(String[] args) {
		
		TransactionProcessor batch = new TransactionProcessor();
		
	    if ( (args.length == 0 || args[0].indexOf("-help")>=0)) {
	        System.out.println(TransactionProcessor.useString);
	        System.exit(0);
	    }
	    
	    batch.processCommandLineArgs(args);
	    
	    batch.initializeLogging();
	    
	    batch.doWork();
	}

    protected void processCommandLineArgs(String[] args) {
    	String propertyFileNameNew = this.propertyFileName;
    	
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-propertyFile")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    try {
                    	System.out.println("Setting property file to: " + value);
                    	propertyFileNameNew = value;
                    }
                    catch (Exception e) {
                    	e.printStackTrace();
                    	System.out.println(TransactionProcessor.useString);
                    	System.exit(0);
					}
                }
            }
            else if (args[i].equalsIgnoreCase("-mode")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    if ("reprocess".equalsIgnoreCase(value)){
                        this.setBatchMode(true);
                    }
                }
            }
            else if (args[i].equalsIgnoreCase("-batchid")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setBatchID(value);
                }
            }
        }

        
    	try {
    		
			this.setPropertyFileName(propertyFileNameNew);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(TransactionProcessor.useString);
        	System.exit(0);
		}

        // verify that if in reprocess mode a batch id exists
        if (this.isBatchMode()) {
        	if (this.batchID == null) {
        		System.out.println("Error Running Transaction processor: -batchid parameter required when reprocessing a batch.");
            	System.out.println(TransactionProcessor.useString);
            	System.exit(0);
        	}
        }
    }
	
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public TransactionProcessor() {
		super();
	}

	public String getPropertyFileName() {
		return propertyFileName;
	}

	public HashMap<String, HashMap<String, MutableInt>> getProcessingStats() {
		return processingStats;
	}

	public boolean isBatchMode() {
		return batchMode;
	}

	public void setBatchMode(boolean batchMode) {
		this.batchMode = batchMode;
	}
    public String getDataOutputDirName() {
		return dataOutputDirName;
	}

	public void setDataOutputDirName(String dataOutputDirName) {
		this.dataOutputDirName = dataOutputDirName;
	}

	public String getBaseOuputFileName() {
		return baseOuputFileName;
	}

	public void setBaseOuputFileName(String baseOuputFileName) {
		this.baseOuputFileName = baseOuputFileName;
	}

	public void setPropertyFileName(String propertyFileName) throws Exception {
		this.propertyFileName = propertyFileName;
        
		//System.out.println("Using Property File: " + this.propertyFileName);
        if (this.propertyFileName != null) {
            java.io.File f = new File(this.propertyFileName);
            if (!f.exists() || !f.isFile()) {
                throw new Exception ("Property File does not exist or is not a file: " + propertyFileName);
            }
            this.loadProperties();
        }
	}

	public String getAlertRecipients() {
		return alertRecipients;
	}

	public void setAlertRecipients(String alertRecipients) {
		this.alertRecipients = alertRecipients;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public Logger getProcessLog() {
		return processLog;
	}

	public ArrayList<String> getProcessingMessages() {
		return processingMessages;
	}

	public int getNumberOfDaysBeforePurge() {
		return numberOfDaysBeforePurge;
	}

	public void setNumberOfDaysBeforePurge(int numberOfDaysBeforePurge) {
		this.numberOfDaysBeforePurge = numberOfDaysBeforePurge;
	}

	public String getLogFileDirName() {
		return logFileDirName;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}

	public ArrayList<String> getAlertMessages() {
		return alertMessages;
	}

	public void setAlertMessages(ArrayList<String> alertMessages) {
		this.alertMessages = alertMessages;
	}

	public void setLogFileDirName(String logFileDirName) {
		this.logFileDirName = logFileDirName;
	}

	public String getGpgHomeDir() {
		return gpgHomeDir;
	}

	public void setGpgHomeDir(String gpgHomeDir) {
		this.gpgHomeDir = gpgHomeDir;
	}

	public String getGpgCommand() {
		return gpgCommand;
	}

	public void setGpgCommand(String gpgCommand) {
		this.gpgCommand = gpgCommand;
	}

	public boolean isEncryptPlainTextFile() {
		return encryptPlainTextFile;
	}

	public void setEncryptPlainTextFile(boolean encryptPlainTextFile) {
		this.encryptPlainTextFile = encryptPlainTextFile;
	}

	public String getGpgKeyAlias() {
		return gpgKeyAlias;
	}

	public void setGpgKeyAlias(String gpgKeyAlias) {
		this.gpgKeyAlias = gpgKeyAlias;
	}

	public String getFTPHost() {
		return FTPHost;
	}

	public void setFTPHost(String fTPHost) {
		FTPHost = fTPHost;
	}

	public String getFtpUserId() {
		return ftpUserId;
	}

	public void setFtpUserId(String ftpUserId) {
		this.ftpUserId = ftpUserId;
	}

	public String getFtpPwd() {
		return ftpPwd;
	}

	public void setFtpPwd(String ftpPwd) {
		this.ftpPwd = ftpPwd;
	}

	public String getSftpHost() {
		return sftpHost;
	}

	public void setSftpHost(String sftpHost) {
		this.sftpHost = sftpHost;
	}

	public String getSftpUserId() {
		return sftpUserId;
	}

	public void setSftpUserId(String sftpUserId) {
		this.sftpUserId = sftpUserId;
	}

	public String getSftpPwd() {
		return sftpPwd;
	}

	public void setSftpPwd(String sftpPwd) {
		this.sftpPwd = sftpPwd;
	}

	public boolean isDeletePlainTextFile() {
		return deletePlainTextFile;
	}

	public void setDeletePlainTextFile(boolean deletePlainTextFile) {
		this.deletePlainTextFile = deletePlainTextFile;
	}

	public boolean isDeleteArchiveFile() {
		return deleteArchiveFile;
	}

	public void setDeleteArchiveFile(boolean deleteArchiveFile) {
		this.deleteArchiveFile = deleteArchiveFile;
	}

	public boolean isSendReport() {
		return sendReport;
	}

	public void setSendReport(boolean sendReport) {
		this.sendReport = sendReport;
	}

	public String getReportRecipients() {
		return reportRecipients;
	}

	public void setReportRecipients(String reportRecipients) {
		this.reportRecipients = reportRecipients;
	}

	public int getRecordsProcessed() {
		return recordsProcessed;
	}

	public void setRecordsProcessed(int recordsProcessed) {
		this.recordsProcessed = recordsProcessed;
	}

	public Level getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(Level logLevel) {
		this.logLevel = logLevel;
	}

	/**
	 * 
	 */
    private void loadProperties() {

        try {
            
            File pFile = new File(this.getPropertyFileName());

            URI uri = pFile.toURI();
            URL url = uri.toURL();
                        
            Properties p = new Properties();
            p.load(url.openStream());
            
            Enumeration<Object> enumer = p.keys();
            while(enumer.hasMoreElements()) {
                String key = (String)enumer.nextElement();
                String value = p.getProperty(key);
                if (key.equalsIgnoreCase("eftpServer")) {
                    this.setFTPHost(value);
                }
                else if (key.equalsIgnoreCase("eftpUser")) {
                    this.setFtpUserId(value);
                }
                else if (key.equalsIgnoreCase("eftpPwd")) {
                    this.setFtpPwd(value);
                }
                if (key.equalsIgnoreCase("esftpServer")) {
                    this.setSftpHost(value);
                }
                else if (key.equalsIgnoreCase("esftpUser")) {
                    this.setSftpUserId(value);
                }
                else if (key.equalsIgnoreCase("esftpPwd")) {
                    this.setSftpPwd(value);
                }
                else if (key.equalsIgnoreCase("useSFTP")) {
                    this.setUseSftp(value);
                }
                else if (key.equalsIgnoreCase("transactionbatch_alertReceviers")) {
                    this.setAlertRecipients(value);
                }
                else if (key.equalsIgnoreCase("transactionbatch_daysBeforePurge")) {
                    try {
                        int numDays = Integer.valueOf(value).intValue();
                        if (numDays < 7) {
                        	System.out.println("Invalid property file setting for number of days before purge. Leaving default value of 14.");
                        	numDays = 14;
                        }
                        this.setNumberOfDaysBeforePurge(numDays);
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for number of days before purge. Leaving default value of 14.");
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_logFileDirectory")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
	                        // attempt to create the log file directory
	                        if (f.mkdir()) {
	                            System.out.println("Created new Log File Directory: " + f.getAbsolutePath());
	                        }
	                        else {
	                        	System.out.println("Failed to create new Log File Directory: " + f.getAbsolutePath());
	                        }
	                    }
	                    else {
	                    	System.out.println("Log File Directory Already Exists: " + f.getAbsolutePath());
	                    }
                        this.setLogFileDirName(value);	                    
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for log file directory. Leaving default value of " + this.getLogFileDirName());
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_archiveDirectory")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
	                        // attempt to create the log file directory
	                        if (f.mkdir()) {
	                            System.out.println("Created new Archive Directory: " + f.getAbsolutePath());
	                            this.setDataOutputDirName(value);
	                        }
	                        else {
	                            System.out.println("Failed to create archive directory using default: " + this.getDataOutputDirName());
	                        }
	                    }
	                    else {
	                        this.setDataOutputDirName(value);
	                    }
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for arhive file directory. Leaving default value of " + this.getDataOutputDirName());
                    }
                }
                else if (key.equalsIgnoreCase("tranactionbatch_gpgEncryptCommand")) {
                    if (value == null || value.length() > 0) {
                        this.setGpgCommand(value);
                    }                	
                }
                else if (key.equalsIgnoreCase("tranactionbatch_gpgEncryptDataFile")) {
                    if (value == null || value.equalsIgnoreCase("false")) {
                        this.setEncryptPlainTextFile(false);
                    }                	
                }
                else if (key.equalsIgnoreCase("transactionbatch_gpgHomeDir")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
                            System.out.println("WARNING: Specified GPG HOMEDIR (transactionbatch_gpgHomeDir) is not valid: " + value);
	                    }
	                    else {
	                        this.setGpgHomeDir(value);
	                    }
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for transactionbatch_gpgHomeDir. Leaving default value of " + this.getGpgHomeDir());
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_gpgKeyAlias")) {
                    this.setGpgKeyAlias(value);
                }
                else if (key.equalsIgnoreCase("transactionbatch_deletePlainTextFile")) {
                    if (value == null || !value.equalsIgnoreCase("false")) {
                        this.setDeletePlainTextFile(true);
                    }
                    else {
                        this.setDeletePlainTextFile(false);
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_deleteArchiveFile")) {
                    if (value != null && value.equalsIgnoreCase("false")) {
                        this.setDeletePlainTextFile(false);
                    }
                    else {
                        this.setDeletePlainTextFile(true);
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_sendEmailReport")) {
                    if (value != null && value.equalsIgnoreCase("false")) {
                        this.setSendReport(false);
                    }
                    else {
                        this.setSendReport(true);
                    }
                }
                else if (key.equalsIgnoreCase("transactionbatch_reportRecipients")) {
                    this.setReportRecipients(value);
                }
                else if (key.equalsIgnoreCase("transactionbatch_logLevel")) {
                    try {
                        this.logLevel = Level.parse(value);
                    }
                    catch (Exception e) {
                        // use default
                        System.out.println("Failed to set logging level, invalid level used. Valid values are found in java.util.logging.Level class. Going with Default Level: " + this.logLevel.getName());
                    }
                }
            } // end while more elements
            
            // initialize UTCommon
           // UTCommon.getInitValues();
        }
        catch (Exception e) {
            System.out.println("TransactionProcessor::loadProperties(): " + e.getMessage());
            e.printStackTrace();
        }
    }
	
    public void initializeLogging() {
        try {
            if (this.getLogFileDirName() != null && this.getLogFileDirName().length() > 0) {
                
            }
            else {
            	System.out.println("Set Log File Directory to '.' in initializeLogging()");
                this.setLogFileDirName(".");
            }
            
            this.processLog = Logger.getLogger("com.usatoday.batch.transactions.TransactionProcessor");

            this.processLog.setLevel(this.logLevel);
            
            this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            
            Calendar cal = Calendar.getInstance();
	        String time = com.usatoday.businessObjects.util.ZeroFilledNumeric.getValue(4, cal.get(Calendar.YEAR)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY));
	        FileHandler f = new FileHandler(this.getLogFileDirName()+ File.separator + "XTRNTDWLD_" + time + ".log", true);
	        
	        System.out.println("Creating log file: " + this.getLogFileDirName()+ File.separator + "XTRNTDWLD_" + time + ".log");
	        
	        f.setFormatter(new java.util.logging.SimpleFormatter());
            this.processLog.addHandler(f);
            
            this.processLog.log(Level.CONFIG, "Transaction Batch Processor Initializing...");
            this.processLog.config("Application started with a property file specified. " + this.getPropertyFileName());

            this.processLog.log(Level.CONFIG, "Log Level: " + this.logLevel.toString());

            this.processLog.fine("FTP Server: "+ this.getFTPHost());
            this.processLog.fine("FTP User: " + this.getFtpUserId());
            this.processLog.finest("FTP Password: " + this.getFtpPwd());
            this.processLog.fine("sFTP Server: "+ this.getSftpHost());
            this.processLog.fine("sFTP User: " + this.getSftpUserId());
            this.processLog.finest("sFTP Password: " + this.getSftpPwd());
            this.processLog.fine("Alert Receivers: " + this.alertRecipients);
            this.processLog.fine("Days Before Purge: " + this.getNumberOfDaysBeforePurge());
            this.processLog.fine("Log File Directory: " + this.getLogFileDirName());
            this.processLog.fine("Archive Directory: " + this.getDataOutputDirName());
            this.processLog.fine("GNUPG Home Directory: " + this.getGpgHomeDir());
            this.processLog.fine("GNUPG Receiver Alias: " + this.getGpgKeyAlias());
            this.processLog.fine("Delete Archive Plain Text Output File: " + this.isDeletePlainTextFile());
            this.processLog.fine("Delete Encrypted Archive Data File: " + this.isDeleteArchiveFile());
            this.processLog.fine("Send Email Report: " + isSendReport());
            this.processLog.fine("Email Report Recipients: " + this.getReportRecipients());
        }
        catch (Exception e) {
            if (this.processLog == null) {
                this.processLog = Logger.getAnonymousLogger();
                this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            }
            this.processLog.severe("Failed to initialize logging for Transaction Batch Processor! " + e.getMessage());
        }
    }
    
    /**
     * 
     */
	private void doWork() {

	    this.getProcessLog().entering(this.getClass().getName(), "doWork");
	    
	    boolean sendAlert = false;
	    
	    if (this.workDone) {
		    this.getProcessLog().warning("Call to doWork more than one time. Doing nothing.");
	        return;
	    }
	    else {
	        this.workDone = true;
	    }
	    
	    this.startTime = new DateTime();
	    
	    SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();
	    
	    int transactionsToProcess = 0;
	    int rowsAffected = 0;
	    
	    Collection<ExtranetSubscriberTransactionIntf> transactions = null;
	    
	    // change state of premiums to in batch processing
	    try {
	    	// if reprocessing a batch
	    	if (this.isBatchMode()) {
	            this.getProcessLog().severe("Reprocessing batch with id: " + this.getBatchID());
	    		transactions = transBO.getTransactionsInBatch(this.getBatchID());
	    		transactionsToProcess = transactions.size();
		        this.getProcessLog().info("Number of Transaction Records to re-process: " + transactionsToProcess);
	    	}
	    	else {
	    		transactionsToProcess = transBO.getCountOfNewTransactions();
		    	if (transactionsToProcess > 0){
		    		transactions = transBO.getNewTransactionsForProcessing();
		    		transactionsToProcess = transactions.size();
		    	}
		        this.getProcessLog().info("Number of Transaction Records to process: " + transactionsToProcess);
	    	}
	    }
	    catch (Exception e) {
	        sendAlert = true;
            alertMessages.add("Exception retrieving records to BATCH PROCESS: " + e.getMessage());
            this.getProcessLog().severe("Exception retrieving records to BATCH PROCESS: " + e.getMessage());
            this.getProcessLog().severe("Sending alert and Terminating: " + e.getMessage());
            this.sendAlert();
            System.exit(0);
        }

	    java.io.File dataFile = null;
	    String time = null;
	    // process the records to the file
	    try {
	        Calendar cal = Calendar.getInstance();
	        time = ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MINUTE)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.SECOND));
	        String pathSepStr = File.separator;
	        if (!this.getDataOutputDirName().endsWith(pathSepStr)) {
	            this.setDataOutputDirName(this.getDataOutputDirName() + pathSepStr);
	        }
	        java.io.File f = new File(this.getDataOutputDirName()+this.getBaseOuputFileName()+ time);

	        this.getProcessLog().info("Created temporary data file: " + f.getAbsolutePath());

	        // logic to create a unique file
	        while (f.exists()) {
		        cal = Calendar.getInstance();
		        time = ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MINUTE));  
		        f = new File(this.getDataOutputDirName()+this.getBaseOuputFileName()+ time);
		        this.getProcessLog().fine("File already existed. Created new temporary data file: " + f.getAbsolutePath());
	        }
	        
	        dataFile = f;
	        
	        PrintWriter oFile = new PrintWriter(new java.io.FileOutputStream(f));
	        
	        if (transactions != null) {
		        for( ExtranetSubscriberTransactionIntf tran : transactions) {
		        	
		            try {
		                String formattedOutputString = tran.getTransactionCSVExportString();
		                
		                oFile.print(formattedOutputString);
		                oFile.println();
		                
		                recordsProcessed++;
		                
		                if (this.getBatchID() == null) {
		                	this.setBatchID(tran.getBatchID());
		                }
	
		                String pub = tran.getPubCode();
		                String tranType = tran.getTransactionRecType();
	
		                
		                HashMap<String,MutableInt> pubStats = this.getProcessingStats().get(pub);
	
		                MutableInt stat = null;
		                
		                if (pubStats == null) {
		                	// no stats for this pub yet
		                	pubStats = new HashMap<String, MutableInt>();
		                	stat = new MutableInt(1);
		                	pubStats.put(tranType, stat);
		                	this.getProcessingStats().put(pub, pubStats);
		                }
		                else {
		                	//have stats for pub
		                	stat = pubStats.get(tranType);
		                	if (stat == null) {
		                		// no stats for this tranrec type yet
		                		stat = new MutableInt(1);
		                		pubStats.put(tranType, stat);
		                	}
		                	else {
		                		int count = stat.intValue();
		                		count++;
		                		stat.setValue(count);
		                	}
		                }
		                
		                // processed versus attribute records processed.
		                this.getProcessLog().finest("Processed Record with Key: " + tran.getID());
		                this.getProcessLog().finest("Formatted String: " + formattedOutputString);	                
		            }
		            catch (com.usatoday.UsatException ue) {
		                sendAlert = true;
		                String errMsg = "Failed to process attribute with key = " + tran.getID() + " Batch ID: " + tran.getBatchID() + "   Error: " + ue.getMessage();
		            	alertMessages.add(errMsg);
		            	this.getProcessLog().severe(errMsg);
	                }
	            } // end for
	        } // end if transaction != null
	        
	        oFile.close();
	        
	        if (recordsProcessed == 0) {
	            f.delete();
	            this.getProcessLog().warning("No records processed..deleted temporary data file.");
	        }
	    }
	    catch (Exception e) {
            alertMessages.add("Unexpected Exception processing records: " + e.getMessage());
            sendAlert = true;
	        this.getProcessLog().severe("Unexpected Exception processing records: " + e.getMessage());
        }
	
	    File encryptedFile = dataFile;
	    if (this.isEncryptPlainTextFile() && recordsProcessed > 0) {
		    encryptedFile = new File(dataFile.getAbsolutePath()+".gpg");
		    
		    // encrypt the file
		    try {
		        if (encryptedFile.exists()) {
		            encryptedFile.renameTo(new File(this.getDataOutputDirName()+ "XD_BACKUP.bkp" + time));
		            encryptedFile = new File(dataFile.getAbsolutePath()+".gpg");
		        }
		        
		        if (this.getRecordsProcessed()>0) {
			        Runtime rt = Runtime.getRuntime();
			        this.getProcessLog().info("Forking process: '" + this.getGpgCommand() + " --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath() + "'");
			       
			        Process p = rt.exec(this.getGpgCommand() + " --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath());
			        //Process p = rt.exec("gpg --always-trust --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath());
			        
			        int exitCode = p.waitFor();
			        this.getProcessLog().fine("Encryption process finished with exit code: " + exitCode);
			        if (exitCode != 0){
			            // failed to run process
			            throw new com.usatoday.UsatException("Failed to encrypt file, gpg process ended with exit code: " + exitCode);
			        }
		        }
		    }
		    catch (Exception e) {
		        sendAlert = true;
	            alertMessages.add("Failed to encrypt XD (Subsriber Transactions) data file: " + e.getMessage());
	            this.getProcessLog().severe("Failed to encrypt XD (Subsriber Transactions) data file: " + e.getMessage());
	        }
	    }
	    
	    // sftp or ftp the file
	    if (recordsProcessed > 0){
	        if (useSftp.equals("Y")) {
		        if(!this.sftpFile(encryptedFile)) 
		        {	            
		        	sendAlert = true;
		            alertMessages.add("Failed to SFTP encrypted data file. MANUAL SEND REQUIRED. File Name: " + encryptedFile.getAbsolutePath());
		        }	        	        	
	        } else if(!this.ftpFile(encryptedFile))
	        {	            
	        	sendAlert = true;
	            alertMessages.add("Failed to FTP encrypted data file. MANUAL SEND REQUIRED. File Name: " + encryptedFile.getAbsolutePath());
	        }	        
	    }

        if (this.isDeletePlainTextFile()) {
            dataFile.delete();
            this.getProcessLog().info("Deleted temporary data file.");		                
        }
	    
	    // update state to processed
	    try {
	        if (recordsProcessed > 0) {
	        	if (this.isBatchMode()) {
		            this.getProcessLog().fine("Changing state of records to ReProcessed... ");
		            rowsAffected = transBO.setTransactionStateOfBatchToReProcessed(this.getBatchID());
		            this.getProcessLog().info("Completed changing state of records to ReProcessed, number rows: " + rowsAffected);
	        	}
	        	else {
		            this.getProcessLog().fine("Changing state of records to Processed... ");
		            rowsAffected = transBO.setTransactionStateFromProcessingToProcessed();
		            this.getProcessLog().info("Completed changing state of records to Processed, number rows: " + rowsAffected);
	        	}
	        }
	    }
	    catch (Exception e) {
	        sendAlert = true;
	        if (this.isBatchMode()) {
		        alertMessages.add("Failed to update State of Batch Transaction records to REPROCESSED. " + e.getMessage());
		        this.getProcessLog().severe("Failed to update State of Batch Transaction records to REPROCESSED " + e.getMessage());
	        }
	        else {
		        alertMessages.add("Failed to update State of Batch Transaction records to PROCESSED. " + e.getMessage());
		        this.getProcessLog().severe("Failed to update State of Batch Transaction records to PROCESSED " + e.getMessage());
	        }
        }
	    
	    // delete old records if normal processing mode
	    if (!this.isBatchMode()) {
		    if (!this.purgeOldRecords(transBO)) {
		        sendAlert = true;
		    }
	    }
	    
	    // Send any reports
	    this.endTime = new DateTime();
	    
	    this.sendReport();
	    
	    // Send any alerts
	    if (sendAlert) {
	        this.sendAlert();
	    }
	    this.getProcessLog().exiting(this.getClass().getName(), "doWork");
	    this.getProcessLog().log(Level.CONFIG, "Application terminating Normally.");
	    
	    // close log files
		Handler[] handlers = this.getProcessLog().getHandlers();
		for (int i = 0; i < handlers.length; i++) {
			Handler h = handlers[i];
			this.getProcessLog().removeHandler(h);
			h.close();
		}
	}

	/**
	 * 
	 * @param transBO
	 * @return
	 */
	private boolean purgeOldRecords(SubscriberTransactionBatchBO transBO) {
	    this.getProcessLog().entering(this.getClass().getName(), "purgeOldRecords");
	    boolean deleteSuccess = true;
	    try {
	        int numDeleted = transBO.purgeTransactionsOlderThan(this.getNumberOfDaysBeforePurge());
	        this.getProcessLog().info("Number of Transaction records purged: " + numDeleted);
	        this.getProcessingMessages().add("Number of Transaction records purged: " + numDeleted);
	    }
	    catch (com.usatoday.UsatException e) {
	        deleteSuccess = false;
	        alertMessages.add("Failed to purge old transactions: " + e.getMessage());
	        this.getProcessLog().severe("Failed to purge old transactions: " + e.getMessage());
        }
	    this.getProcessLog().exiting(this.getClass().getName(), "purgeOldRecords");
	    return deleteSuccess;
	}

	private boolean sftpFile (File file){

		boolean sftpSuccess = false;
		// Set license information for SFTP software
		com.enterprisedt.util.license.License.setLicenseDetails("USAToday", "382-6045-2908-7486");
        // we want remote host, user name and password

        try {
            // create client
        	this.getProcessLog().fine("Creating SFTP client");
            SSHFTPClient sftp = new SSHFTPClient();

            // set remote host
            sftp.setRemoteHost(sftpHost);

            this.getProcessLog().fine("Setting user-name and password");
            sftp.setAuthentication(sftpUserId, sftpPwd);

            this.getProcessLog().fine("Turning off server validation");
            sftp.getValidator().setHostValidationEnabled(false);

            // connect to the server
            this.getProcessLog().fine("Connecting to server " + sftpHost);
            sftp.connect();

            this.getProcessLog().fine("Setting transfer mode to BINARY");
            sftp.setType(FTPTransferType.BINARY);

            sftp.chdir("incoming");
			// if a source file is found then publish it
			if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				this.getProcessLog().info("SFTPing file: " + file.getAbsolutePath());
				sftp.put(fis, file.getName());
	            this.getProcessLog().info("Successfully transferred in BINARY mode");
			}

            // Shut down client
            this.getProcessLog().fine("Quitting client");
            sftp.quit();
            sftpSuccess = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return sftpSuccess;
    }
	/**
	 * This method should be deleted once we switch to sftp method
	 * 
	 * @param file
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private boolean ftpFile(File file) {
	    this.getProcessLog().entering(this.getClass().getName(), "ftpFile");
	    boolean ftpSuccess = true;
	    
		com.enterprisedt.net.ftp.FTPClient ftp = null;

		try {
			ftp = new com.enterprisedt.net.ftp.FTPClient(this.getFTPHost());

			com.enterprisedt.net.ftp.FTPMessageCollector listener = new com.enterprisedt.net.ftp.FTPMessageCollector();
			ftp.setMessageListener(listener);       

			ftp.login(this.getFtpUserId(),this.getFtpPwd());
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());

			// set up passive ASCII transfers
			ftp.setConnectMode(com.enterprisedt.net.ftp.FTPConnectMode.PASV);
			ftp.setType(com.enterprisedt.net.ftp.FTPTransferType.BINARY);

			ftp.chdir("incoming");
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());
                    
			// if a source file is found then publish it
			if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				this.getProcessLog().info("FTPing file: " + file.getAbsolutePath());
				ftp.put(fis, file.getName());
				this.getProcessLog().info(ftp.getLastValidReply().getReplyText());
			}

			ftp.quit();
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());
                                
		}
		catch (Exception e) {
			if (ftp != null) {
				try {
					ftp.quit();
				} catch (Exception exp) {}
			}
			ftpSuccess = false;
			alertMessages.add("ftpFile() - " + e.getMessage());
			this.getProcessLog().severe("ftpFile() - " + e.getMessage());
		}
	    this.getProcessLog().exiting(this.getClass().getName(), "ftpFile", new Boolean(ftpSuccess));
		
		return ftpSuccess;
	}

	/**
	 * 
	 */
	private void sendReport() {
        StringBuilder msg = new StringBuilder();
        try {
            this.getProcessLog().entering(this.getClass().getName(), "sendReport");
            
            DateTimeFormatter fmt = DateTimeFormat.forPattern("E MMM dd HH:mm:ss z yyyy");
	        EmailAlert eAlert = new EmailAlert();
	        
	        eAlert.setReceiverList(this.getReportRecipients());
	        eAlert.setSender("transactionBatchProcessor@usatoday.com");
	        eAlert.setSubject("Subscriber Portal Download Processing Report");
	        
	        msg.append("Time of Report Generation: " + fmt.print(new DateTime())).append("\n\n");
	        
	        msg.append("If re-processing required use batch id: ").append(this.getBatchID()).append("\n\n");
	        
	        msg.append("Record types being transferred to the AS/400 (XTRNTDWLD)\n\n");
	        msg.append("Transaction Batch Process started at: ").append(fmt.print(this.startTime)).append("\n...");
	        msg.append("\nTransaction Batch Process completed at: ").append(fmt.print(this.endTime)).append("\n\n");
	        
	        msg.append("\n\n").append("Total Transaction Records Processed: ").append(this.getRecordsProcessed()).append("\n");
	        
	        
	        for (String pubKey : this.getProcessingStats().keySet()) {
	        
	        	HashMap<String, MutableInt> pubStats = this.getProcessingStats().get(pubKey);
	        
	        	int pubCount = 0;
	        	msg.append("PubCode ").append(pubKey).append(":\n");
	        	
	        	for (String recType : pubStats.keySet()) {
	        		MutableInt stat = pubStats.get(recType);
	        		
	        		// add to total for pub
	        		pubCount+= stat.intValue();
	        		
	        		msg.append("\tTransaction Type ").append(recType).append(":\t").append(stat.intValue()).append("\n");
	        	}
	        	msg.append("\tTotal For Pub:\t\t").append(pubCount).append("\n");
	        }
	        
	        if (this.getProcessingMessages().size() > 0 ){
	            msg.append("\nProcessing Detail Message(s): ").append("\n=================================\n");
		        for (Iterator<String> iter = processingMessages.iterator(); iter.hasNext();) {
		            String element = iter.next();
		            msg.append(element).append("\n");
		        }
	        }
	        
	        this.getProcessLog().fine("Report Message: " + msg.toString());
	        
	        eAlert.setBodyText(msg.toString());
	        eAlert.sendAlert();
	        this.getProcessLog().exiting(this.getClass().getName(), "sendReport");
        }
        catch (Exception e) {
	        this.getProcessLog().severe("Report Message: " + msg.toString());
            this.getProcessLog().severe("sendReport() - Error sending transaction report! " + e.getMessage());
        }
    }
    
    /**
     * Sends our alerts to alert list
     *
     */
    private void sendAlert() {
        try {
            this.getProcessLog().entering(this.getClass().getName(), "sendAlert");
            
	        EmailAlert eAlert = new EmailAlert();
	        eAlert.setReceiverList(this.getAlertRecipients());
	        eAlert.setSender("transactionBatchProcessor@usatoday.com");
	        eAlert.setSubject("Alert from Transaction Batch Processor");
	        
	        StringBuffer msg = new StringBuffer();
	        msg.append("Time of Alert: " + Calendar.getInstance().getTime().toString());
	        msg.append("\n\n").append("Alert Messages:\n").append("==============\n\n");
	        
	        for (Iterator<String> iter = alertMessages.iterator(); iter.hasNext();) {
	            String element = iter.next();
	            msg.append(element).append("\n");	            
	        }
	        
	        this.getProcessLog().info("Alert Message: " + msg.toString());
	        
	        eAlert.setBodyText(msg.toString());
	        eAlert.sendAlert();
	        
            this.getProcessLog().exiting(this.getClass().getName(), "sendAlert");
        }
        catch (Exception e) {
            System.out.println("sendAlert() - Error sending alert! " + e.getMessage());
        }
        
    }

	public String getUseSftp() {
		return useSftp;
	}

	public void setUseSftp(String useSftp) {
		this.useSftp = useSftp.trim();
	}
	
}